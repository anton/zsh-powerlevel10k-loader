
# shelldon

Automatically set up ZSH Shell, Oh my ZSH and Powerlevel 10K whilst SSHing into hosts

## Install

1. Clone this project

   ```shell
   git clone git@gitlab.com:anton/shelldon.git ~/.shelldon
   ```

1. On the SSH client, add the following to the `~/.ssh/config` file:

   ```shell
   Host * !gitlab.com !github.com !bitbucket.com
    PermitLocalCommand yes
    LocalCommand cat ~/.shelldon/loader.sh | ssh -o PermitLocalCommand=no %n /bin/sh
   ```