#!/bin/bash

printf "\033[0;32mStarting Shelldon...\033[0m\n"

# Function to check if Zsh is the default shell
is_zsh_default() {
  [ "$SHELL" = "/usr/bin/zsh" ] || [ "$SHELL" = "/bin/zsh" ]
}

# Function to install and configure Zsh
setup_zsh() {
  printf "\033[0;36mZsh is not the default shell. Running setup...\033[0m\n"

  # Install Zsh
  printf "\033[0;36mInstalling Zsh...\033[0m\n"
  sudo apt -y install zsh

  # Install Oh My Zsh
  if [ ! -d "$HOME/.oh-my-zsh" ]; then
    printf "\033[0;36mInstalling Oh My Zsh...\033[0m\n"
    sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  else
    printf "\033[0;36mOh My Zsh is already installed. Skipping.\033[0m\n"
  fi

  # Install Powerlevel 10K
  if [ ! -d "${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k" ]; then
    printf "\033[0;36mInstalling Powerlevel10k theme...\033[0m\n"
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
  else
    printf "\033[0;36mPowerlevel10k configuration template already exists. Updating...\033[0m\n"
    git -C ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k pull
  fi

  # Set the theme to Powerlevel 10K
  if ! grep -q "powerlevel10k/powerlevel10k" ~/.zshrc; then
    printf "\033[0;36mSetting Powerlevel10k as the default theme...\033[0m\n"
    sed -i 's/robbyrussell/powerlevel10k\/powerlevel10k/g' ~/.zshrc
  else
    printf "\033[0;36mPowerlevel10k theme is already set. Skipping.\033[0m\n"
  fi

  # Download the Powerlevel 10K template
  if [ ! -d "${ZSH_CUSTOM:-$HOME}/.shelldon" ]; then
    printf "\033[0;36mDownloading Powerlevel10k configuration template...\033[0m\n"
    git clone --depth=1 https://gitlab.com/anton/shelldon.git ${ZSH_CUSTOM:-$HOME}/.shelldon
    ln -s ${ZSH_CUSTOM:-$HOME}/.shelldon/.p10k.zsh ${ZSH_CUSTOM:-$HOME}/.p10k.zsh
  else
    printf "\033[0;36mPowerlevel10k configuration template already exists. Updating...\033[0m\n"
    git -C ${ZSH_CUSTOM:-$HOME}/.shelldon pull
  fi

  # Activate Powerlevel 10K
  if ! grep -q "\[ -f ~/.p10k.zsh \] && source ~/.p10k.zsh" ~/.zshrc; then
    printf "\033[0;36mActivating Powerlevel10k in .zshrc...\033[0m\n"
    cat <<EOT >> ~/.zshrc

# To customize prompt, run 'p10k configure' or edit ~/.p10k.zsh.
[ -f ~/.p10k.zsh ] && source ~/.p10k.zsh
EOT
  else
    printf "\033[0;36mPowerlevel10k is already activated in .zshrc. Skipping.\033[0m\n"
  fi

  # Set Zsh as the default shell
  if ! is_zsh_default; then
    printf "\033[0;36mSetting Zsh as the default shell...\033[0m\n"
    if sudo chsh -s $(which zsh) $USER; then
      printf "\033[0;36mDefault shell changed to Zsh.\033[0m\n"
    else
      printf "\033[0;36mFailed to change default shell to Zsh. Please run 'sudo chsh -s $(which zsh) $USER' manually.\033[0m\n"
      return 1
    fi
  else
    printf "\033[0;36mZsh is already the default shell.\033[0m\n"
  fi

  # Install fast-stats
  printf "\033[0;36mDownloading fast-stats...\033[0m\n"
  FAST_STATS_URL="https://gitlab.com/-/project/12295093/uploads/b4a55dcba6495ff29249f755359b9823/fast-stats_0-8-4_linux_x86_64_musl.tar.gz"
  curl -s -L -o "/tmp/fast-stats.tar.gz" "$FAST_STATS_URL"
  tar -xzf "/tmp/fast-stats.tar.gz" -C "/tmp/"
  sudo mv "/tmp/fast-stats" /usr/bin/

  # Prompt user to sign out and sign back in
  printf "\033[0;36mSetup complete. Please sign out and sign back in to start using Zsh as your default shell.\033[0m\n"
}

# Run setup if Zsh is not the default shell
if ! is_zsh_default; then
  setup_zsh
else
  printf "\033[0;36mZsh is already the default shell. No setup needed.\033[0m\n"
fi

printf "\033[0;32mShelldon finished.\033[0m\n\n"